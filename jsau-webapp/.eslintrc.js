'use strict'
module.exports = {
    root: true,
    env: {
        node: true,
        'vue/setup-compiler-macros': true
    },
    "parserOptions": {
        parser: "@babel/eslint-parser"
    },
    extends: [
        "plugin:vue/vue3-recommended",
        'eslint:recommended'
    ]
}