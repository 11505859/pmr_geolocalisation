# Record Api

To geolocalise a record


    ## API

          ### Default

          |Name|Role|
          |----|----|
          |`org.openapitools.server.api.DefaultController`|akka-http API controller|
          |`org.openapitools.server.api.DefaultApi`|Representing trait|
              |`org.openapitools.server.api.DefaultApiImpl`|Default implementation|

                * `GET /api/record/info` - Get all the record in the database
                * `GET /api/record/info/{id}` - Get a single record in the database

