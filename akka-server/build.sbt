version := "1.0.0"
name := "openapi-scala-akka-http-server"
organization := "org.openapitools"
scalaVersion := "2.13.1"

lazy val akkaHttpVersion = "10.2.6"

lazy val akkaVersion = "2.6.18"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.10",
  //"com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
  "io.spray" %% "spray-json" % "1.3.6",
)
