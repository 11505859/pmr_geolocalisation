package org.openapitools.server.api

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import akka.http.scaladsl.unmarshalling.FromStringUnmarshaller
import org.openapitools.server.AkkaHttpHelper._
import org.openapitools.server.model.Record


class DefaultApi(
    defaultService: DefaultApiService,
    defaultMarshaller: DefaultApiMarshaller
) {

  
  import defaultMarshaller._

  lazy val route: Route =
    path("record" / "info") { 
      get {  
            defaultService.recordInfoGet()
      }
    } ~
    path("record" / "info" / Segment) { (id) => 
      get {  
            defaultService.recordInfoIdGet(id = id)
      }
    }
}


trait DefaultApiService {

  def recordInfoGet200(responseRecordarray: Seq[Record])(implicit toEntityMarshallerRecordarray: ToEntityMarshaller[Seq[Record]]): Route =
    complete((200, responseRecordarray))
  /**
   * Code: 200, Message: to get a list of recorded items, DataType: Seq[Record]
   */
  def recordInfoGet()
      (implicit toEntityMarshallerRecordarray: ToEntityMarshaller[Seq[Record]]): Route

  def recordInfoIdGet200(responseRecord: Record)(implicit toEntityMarshallerRecord: ToEntityMarshaller[Record]): Route =
    complete((200, responseRecord))
  /**
   * Code: 200, Message: Success, Get a single record, DataType: Record
   */
  def recordInfoIdGet(id: String)
      (implicit toEntityMarshallerRecord: ToEntityMarshaller[Record]): Route

}

trait DefaultApiMarshaller {


  implicit def toEntityMarshallerRecordarray: ToEntityMarshaller[Seq[Record]]

  implicit def toEntityMarshallerRecord: ToEntityMarshaller[Record]

}

